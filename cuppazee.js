global.config = JSON.parse(fs.readFileSync('config.json','utf8'));
if(!global.clans) global.clans = {};
if(!global.clanreload) global.clanreload = {};
if(!global.users) global.users = {};
if(!global.usercache) global.usercache = {};
if(!global.ucad) global.ucad = {};
if(!global.boot) global.boot = Math.random().toString(36).substr(2, 5);
global.resetData = function(){
	global.clans = {};
	global.clanreload = {};
	global.users = {};
	global.usercache = {};
	global.ucad = {};
}


// USEFUL INFO!
global.useful = {};
useful.specials = [
	'Jewel Thief Nomad',
	'Nomad',
	'Nomad Virtual',
	'Warrior Nomad',
	'Scattered',
	false
]

global.v = "virtual";
useful.requirements = [null,null,null,null,
	{req: true,clan: [
		{tot_p:85000,cap:200},
		{tot_p:170000,cap:350},
		{tot_p:400000,cap:1000,gam_cap:10},
		{tot_p:600000,cap:2500,gam_cap:25,phy_dep:100},
		{tot_p:850000,cap:5000,gam_cap:75,phy_dep:250}
	],user: [
		{tot_p:6500},
		{tot_p:17500,cap:30},
		{tot_p:35000,cap:75},
		{tot_p:50000,cap:150},
		{tot_p:75000,cap:300}
	]},
	{"req":true,"clan":[
		{"phy_cap":250,"phy_dep":150,"tot_p":100000},
		{"phy_cap":500,"phy_dep":300,"tot_p":250000},
		{"phy_cap":800,"phy_dep":400,"tot_p":500000},
		{"phy_cap":1100,"phy_dep":700,"tot_p":999999},
		{"phy_cap":2000,"phy_dep":1000,"myth_cap":50,"tot_p":1500000}
	],"user":[
		{"tot_p":6000},
		{"tot_p":15000,"phy_cap":20},
		{"tot_p":35000,"phy_cap":50,"phy_dep":25},
		{"tot_p":50000,"phy_cap":70,"phy_dep":50},
		{"tot_p":100000,"phy_cap":100,"phy_dep":80}
	]},
	{"req":true,"clan":[
		{"c_days":7,"tot_p":100000},
		{"c_days":10,"d_days":7,"tot_p":225000},
		{"c_days":14,"d_days":14,"clan_dep":100,"tot_p":450000},
		{"c_days":21,"d_days":21,"clan_dep":175,"jewel_dep":100,"tot_p":700000},
		{"c_days":28,"d_days":28,"clan_dep":200,"jewel_dep":200,"tot_p":1000000}
	],"user":[
		{"c_days":7,"tot_p":5000},
		{"c_days":10,"d_days":7,"tot_p":12000},
		{"c_days":14,"d_days":14,"tot_p":30000},
		{"c_days":21,"d_days":21,"tot_p":50000},
		{"c_days":28,"d_days":28,"tot_p":85000}
	]},
	{"req":true,"clan":[
		{"tot_p":40000,"cap_xsoc":125,"dep_xsoc":30},
		{"tot_p":90000,"cap_xsoc":250,"dep_xsoc":75},
		{"tot_p":175000,"cap_xsoc":400,"dep_xsoc":150},
		{"tot_p":325000,"cap_xsoc":750,"dep_xsoc":250},
		{"tot_p":625000,"cap_xsoc":1500,"dep_xsoc":400}
	],"user":[
		{"tot_p":2500},
		{"tot_p":7000},
		{"tot_p":15000},
		{"tot_p":25000},
		{"tot_p":42500}
	],
	"rewards":[
		
	]},
	{"req":true,"clan":[
		{"tot_p":100000,"mys_cap":100,"mothot_cap":0,"mys_dep":0,"phyjew_cap":0,"phyevo_cap":0,"prem_cap":0,"phyjew_dep":0,"phyevo_dep":0,"prem_dep":0,"mothot_dep":0},
		{"tot_p":200000,"mys_cap":100,"mothot_cap":200,"mys_dep":20,"phyjew_cap":0,"phyevo_cap":0,"prem_cap":0,"phyjew_dep":0,"phyevo_dep":0,"prem_dep":0,"mothot_dep":0},
		{"tot_p":450000,"mys_cap":100,"mothot_cap":200,"mys_dep":20,"phyjew_cap":200,"phyevo_cap":0,"prem_cap":0,"phyjew_dep":0,"phyevo_dep":0,"prem_dep":0,"mothot_dep":0},
		{"tot_p":700000,"mys_cap":100,"mothot_cap":200,"mys_dep":20,"phyjew_cap":200,"phyevo_cap":200,"prem_cap":50,"phyjew_dep":0,"phyevo_dep":0,"prem_dep":0,"mothot_dep":0},
		{"tot_p":1000000,"mys_cap":100,"mothot_cap":200,"mys_dep":50,"phyjew_cap":200,"phyevo_cap":200,"prem_cap":50,"phyjew_dep":200,"phyevo_dep":50,"prem_dep":10,"mothot_dep":150}
	],"user":[
		{},
		{},
		{},
		{},
		{}
	]},
	{"name":"Oct 18","clan":[
		{tot_p: 100000, myth_cap: 20, tempvirt_cap: 10, event_cap: 2, rmyth_cap: 0, zeeqrew_replace: 0, virtevo_cap: 50, surprise_cap: 0, scatter_cap: 0, scatter_dep: 0, scattered_cap: 25, prize_cap: 0, clan_dep: 0},
		{tot_p: 250000, myth_cap: 20, tempvirt_cap: 10, event_cap: 5, rmyth_cap: 0, zeeqrew_replace: 10, virtevo_cap: 50, surprise_cap: 20, scatter_cap: 0, scatter_dep: 0, scattered_cap: 25, prize_cap: 25, clan_dep: 50},
		{tot_p: 450000, myth_cap: 40, tempvirt_cap: 10, event_cap: 10, rmyth_cap: 5, zeeqrew_replace: 35, virtevo_cap: 50, surprise_cap: 20, scatter_cap: 50, scatter_dep: 0, scattered_cap: 25, prize_cap: 25, clan_dep: 150},
		{tot_p: 600000, myth_cap: 50, tempvirt_cap: 10, event_cap: 10, rmyth_cap: 10, zeeqrew_replace: 60, virtevo_cap: 50, surprise_cap: 20, scatter_cap: 50, scatter_dep: 10, scattered_cap: 25, prize_cap: 25, clan_dep: 150},
		{tot_p: 800000, myth_cap: 50, tempvirt_cap: 110, event_cap: 15, rmyth_cap: 10, zeeqrew_replace: 100, virtevo_cap: 100, surprise_cap: 20, scatter_cap: 50, scatter_dep: 50, scattered_cap: 125, prize_cap: 125, clan_dep: 250}
	],"user":[
		{tot_p: 5000, clan_dep: 0,myth_cap: 0},
		{tot_p: 7500, clan_dep: 5,myth_cap: 0},
		{tot_p: 12500, clan_dep: 15,myth_cap: 0},
		{tot_p: 30000, clan_dep: 15,myth_cap: 0},
		{tot_p: 75000, clan_dep: 15,myth_cap: 3}
	]}
];
global.reloadMunzeeList = async function(){
	eval(await rp('https://gitlab.com/MOBlox/CuppaZee-2/raw/master/img2type.js?inline=false'));
	useful.munzees = JSON.parse(await rp('https://gitlab.com/MOBlox/CuppaZee-2/raw/master/munzees.json?inline=false'));
}
useful.phyVirt = {
	"flatrob": 0,
	"mysteryvirtual": 0,
	"mvmwhite": 0,
	"flatmatt": 0,
	"virtual": 0,
	"mvmblue": 0,
	"temporaryvirtual": 0,
	"mvmdarkgreen": 0,
	"crossbow": 0,
	"virtualemerald": 0,
	"virtualshamrock": 0,
	"icebucket": 0,
	"mvmred": 0,
	"mvmrainbow": 0,
	"mvmblack": 0,
	"virtualresortroom": 0,
	"flatlou": 0,
	"rmhvirtual": 0,
	"mvmpurple": 0,
	"mvmorange": 0,
	"surprise": 0,
	"virtual_mystery_red": 0,
	"mvmgreen": 0,
	"mvmbrown": 0,
	"mvmpink": 0,
	"mvmyellow": 0,
	"virtualsapphire": 0,
	"virtual_mystery_black": 0,
	"personalmunzee": "X",
	"mystery": 1,
	"virtualamethyst": 0,
	"mvmlightblue": 0,
	"mvmindigo": 0,
	"virtual_mystery_green": 0,
	"virtual_mystery_yellow": 0,
	"virtual_mystery_orange": 0,
	"safaribus": 0,
	"submarine": 0,
	"virtual_mystery_rainbow": 0,
	"musclecar": 0,
	"virtualresort": 0,
	"mwtxusa": 0,
	"standard": 1,
	"eggs": 0,
	"bcagarden": 0,
	"pawgarden": 0,
	"championshiphorse": 0,
	"virtual_mystery_violet": 0,
	"virtual_mystery_indigo": 0,
	"virtual_mystery_brown": 0,
	"pegasus": 0,
	"family": 0,
	"field": 0,
	"virtual_green": 0,
	"virtual_red": 0,
	"carrot": 0,
	"peas": 0,
	"leprechaun": 1,
	"airmystery": 0,
	"dragon": 1,
	"theunicorn": 1,
	"virtual_rainbow": 0,
	"eventzeeeventattendee": "X",
	"bcapin": 0,
	"canoe": 0,
	"virtual_violet": 0,
	"2015heartgreen": 0,
	"earthday2018tree": 0,
	"virtual_black": 0,
	"yeti": 1,
	"virtual_blue": 0,
	"faun": 1,
	"hydra": 1,
	"2015heartorange": 0,
	"2015heartyellow": 0,
	"virtualmysterypink": 0,
	"2015heartpink": 0,
	"virtual_yellow": 0,
	"motelroom": 1,
	"virtual_orange": 0,
	"tuli": 1,
	"farmerandwife": 0,
	"garden": 0,
	"cyclops": "MYTH",
	"virtual_brown": 0,
	"safaritruck": 0,
	"firstwheel": 0,
	"racehorse": 0,
	"hotelroom": 1,
	"farmer": 0,
	"footprint": 0,
	"hoofprint": 0,
	"colt": 0,
	"pottedplant": 0,
	"chick": 0,
	"chicken": 0,
	"motorboat": 0,
	"diamond": 1,
	"pawprint": 0,
	"safarivan": 0,
	"clawprint": 0,
	"talonprint": 0,
	"virtual_indigo": 0,
	"txhistoricallocation": 0,
	"penny-farthingbike": 0,
	"doublepointweekend": 1,
	"vesi": 1,
	"ruby": 1,
	"virtualtrail": 0,
	"carrotseed": 0,
	"prizewheel": 1,
	"candycorn": 0,
	"catapult": 0,
	"peasseed": 0,
	"diabetesawarenesscharity": 0,
	"carrotplant": 0,
	"peasplant": 0,
	"earthday2018seedling": 0,
	"longsword": 1,
	"boulder": 1,
	"fire": 1,
	"nomadvirtual": 0,
	"mapcleanup": 1,
	"mermaid": 1,
	"firemystery": 1,
	"scarypumpkin": 0,
	"worldphotoday2018": 1,
	"battleaxe": 1,
	"mace": 1,
	"goldenfeather": 0,
	"feather": 0,
	"motelmunzee": 1,
	"poiairport": 0,
	"premiumpersonal": "X",
	"thehammer": 1,
	"scatter": 1,
	"hotelmunzee": 1,
	"aquamarinemunzee": 1,
	"6thbirthdaysuperrob": 0,
	"poifaithplace": 0,
	"celebratemunzeefl2016": 0,
	"earthday2018seed": 0,
	"scattered": 1,
	"movember": 0,
	"suncoastii": 0,
	"internationalleftiesday2017": 1,
	"topaz": 1,
	"premium": 1,
	"business": 1,
	"frozengreenie": 1,
	"fatherchristmas2016": 1,
	"lotusyogapose": 0,
	"warrioriiyogapose": 0,
	"shamrock": 1,
	"gogreen2018": 0,
	"gnomeleprechaun": 1,
	"cherub": "MYTH",
	"gold": 0,
	"earofcorn": 1,
	"minthill2015": 0,
	"tomato": 1,
	"cahistoricallocation": 0,
	"exploringmiddletennessee2016": 0,
	"cappingthecoast2014": 0,
	"eventtrail": 0,
	"poicemetery": 0,
	"poihospital": 0,
	"icemystery": 1,
	"poipostoffice": 0,
	"rockpaperscissors": 1,
	"bluemunzee(withoutyou)": 0,
	"flatdaythe13th": 1,
	"maskedflatmatt": 0,
	"pumpkin": 0,
	"easterbunny": 0,
	"robicorn": 0,
	"4thbirthday": 1,
	"waterdroplet": 1,
	"clowncar2018": 1,
	"quizvirtual": 0,
	"poimuseum": 0,
	"easter": 0,
	"timeshareroom": 1,
	"downwarddogyogapose": 0,
	"butte": 0,
	"endoftheworld": 0,
	"dossier": 1,
	"meanie": 1,
	"poisports": 0,
	"poiuniversity": 0,
	"timesharemunzee": 1,
	"mmcocoabeachreseller": 1,
	"stmarysegg": 0,
	"westernaustraliadayinsheppzee": 0,
	"heart": 0,
	"stmarysbacon": 0,
	"staff": "X",
	"munzeemadness2": 0,
	"fridaynightjibby2015": 0,
	"geologgersreseller": 1,
	"eventpin": 1,
	"wdw2014": 0,
	"cobrayogapose": 0,
	"munzeemadnessreseller": 1,
	"spacecoastmunzeefest": 0,
	"munzeen6-vuotissynttäritturussa": 0,
	"poifirstresponders": 0,
	"pinkdiamond": 1,
	"fatherchristmas": 0,
	"firepegasus": "MYTH",
	"2ndbirthday": 0,
	"piratenomad": 1,
	"birthdaycircus2018": 0,
	"shamrock2012": 0,
	"icedragon": 1,
	"hotdogpa2015": 0,
	"fairy": "MYTH",
	"ddcardsreseller": 1,
	"1stannualmunztersmash": 0,
	"blastintheparkii": 0,
	"infraredvirtual": 0,
	"spacecoastcomestotyler,tx": 0,
	"capricorn": 1,
	"poiwildlife": 0,
	"garfieldstatue": 0,
	"qrazyturnsone": 0,
	"franklineveningcelebration2016": 0,
	"worlddiabetesday2017": 0,
	"powerpellet": 1,
	"5thbirthday": 1,
	"averymerryunbirthday2016": 0,
	"forgingcommunity2015": 0,
	"raindrop": 1,
	"heartcapper1": 0,
	"jibbyfest2015": 0,
	"trailmunzee": 1,
	"5thbirthdaygift": 1,
	"rujareseller": 1,
	"geostuffreseller": 1,
	"hellsinkimunzfun": 0,
	"worldscollideseattle": 0,
	"negsreseller": 1,
	"ddcardselkevent": 0,
	"geohobbiesreseller": 1,
	"christmasintexas": 0,
	"heartwrangler2": 0,
	"accessibility": 1,
	"cancer": 1,
	"earthmystery": 1,
	"poihistoricalplace": 0,
	"pisces": 1,
	"huntforthefranklinbeast2016": 0,
	"spectacular6thbirthdayspecial": 0,
	"nomad": 1,
	"nfc": 1,
	"gemini": 1,
	"teaparty2017": 0,
	"fatherchristmas2017": 0,
	"aquarius": 1,
	"flyingpig": 0,
	"gwxiv": 0,
	"birthday": 0,
	"munzeemarketplacecbribboncutting": 0,
	"mhqunderthebigtop7thbirthdaycelebration": 0,
	"mhqflatmatt": 0,
	"scorpio": 1,
	"aries": 1,
	"briefcase": 1,
	"tomatoplant": 1,
	"munzeemeetsowl-birthdaybash3/4": 0,
	"rose": 0,
	"goldencarrot": 1,
	"internationalbacon": 1,
	"coexist": 1,
	"sagittarius": 1,
	"getoutside2016": 0,
	"bacon": 1,
	"towerbridgeflatlou": 0,
	"mhqflatrob": 0,
	"leapday2016": 1,
	"watermystery": 1,
	"australiaday": 1,
	"milehighmischief": 0,
	"berkshiremunzeebirthdaybash": 0,
	"quiznormal": 1,
	"munzeemadnessv": 0,
	"robinnc2015": 0,
	"poilibrary": 0,
	"happy4thbirthdayberlingrunewald": 0,
	"towelday2018": 1,
	"munzeemania2018--pizzaparty": 0,
	"piday2018": 1,
	"bierkrug": 0,
	"munzeenationassembly": 0,
	"munzeemania2018--mainevent": 0,
	"milk": 1,
	"coal": 1,
	"taurus": 1,
	"heartwrangler1": 0,
	"grandopening": 0,
	"virgo": 1,
	"weare5": 0,
	"munzeemeetsowl-birthdaybash4/4": 0,
	"pariscachebash": 0,
	"thanksgiving2016": 0,
	"ghost": 1,
	"egghunt": 0,
	"worldturtleday2017": 1,
	"americanbacon": 1,
	"londontiming": 0,
	"4thbirthdaycaketurtle": 0,
	"4thbirthdaycakestrawberry": 0,
	"quadfrog": 1,
	"gingerbreadman": 0,
	"seaweed": 1,
	"mmcocoabeachgrandopening": 0,
	"christmaspickle": 1,
	"mm3": 0,
	"menorah": 1,
	"thankyoufromlouise2018": 0,
	"happy4thbirthdaykent": 0,
	"goldncoins": 0,
	"munzeemeetsowl-birthdaybash1/4": 0,
	"nomadmystery": 1,
	"eyeball": 0,
	"firework2016": 1,
	"thebacon": 0,
	"lederhosen": 0,
	"tomatoseed": 1,
	"mwmb@muncie": 0,
	"cornstalk": 1,
	"goldencarrotplant": 1,
	"mhqinnz": 0,
	"laupereseller": 1,
	"xboxcontest": 0,
	"australiaday2015": 0,
	"brezel": 0,
	"lakenonafltourdecure": 0,
	"heartcapper2": 0,
	"mhqbash2015": 0,
	"geowoodstock14er": 0,
	"worldheritagehistoricallocation": 0,
	"garlicbreathafterparty": 0,
	"nightvisiongoggles": 0,
	"flsuncoasteventiii": 0,
	"waywardnation": 0,
	"hitchhiker2018": 0,
	"shark": 1,
	"coexist15!": 0,
	"ignitedfirework2016": 0,
	"lioncub": 1,
	"woodenshoe": 0,
	"bones": 1,
	"christmasgift": 0,
	"mhqbash4:deuceswild": 0,
	"cappingthecoastfridaynight": 0,
	"piglet": 1,
	"kingofthejungle": 1,
	"christmas2017sun": 0,
	"christmastree": 0,
	"africkinfindlaychristmasevent": 0,
	"wreath": 0,
	"santa": 0,
	"pinatamint": 1,
	"suncoastmunzeeahoy": 0,
	"wallabashatmhq": 0,
	"foodtruckfandango": 0,
	"zeeopsspecial": 0,
	"munzeemeetsowl-birthdaybash2/4": 0,
	"candycane": 0,
	"hoe": 1,
	"libra": 1,
	"goldencarrotseed": 1,
	"milehighmunzbrews": 0,
	"christmas2017snowflake": 0,
	"tractor": 1,
	"snowman": 0,
	"leo": 1,
	"puzzleheart": 0,
	"thankyoufromrob2018": 0,
	"thankyoufrommatt2018": 0,
	"6thbirthdaypow": 0,
	"guyfawkesday": 1,
	"copadomunzo": 1,
	"scgsreseller": 1,
	"2ndannualcuriositeaparty": 0,
	"pig": 1,
	"flsuncoastpiratefeast": 0,
	"australiaday2017": 0,
	"backtothefutureday": 1,
	"agatheringofthefederation": 0,
	"nightofthelivingmunzee": 0,
	"cow": 1,
	"fish": 1,
	"fthqcirquedumunz7thbirthdaycelebration": 0,
	"alightsaberroguechrismas": 0,
	"bagofcoins": 1,
	"gwxiii": 0,
	"picnicinthepark2017": 0,
	"emg": 0,
	"boxofchocolates2017": 0,
	"remembrance": 1,
	"somethingwickedthiswaymunzees": 0,
	"whompnation": 0,
	"southerncaliforniaaprilteapartyevent": 0,
	"amaymunzeemeetup": 0,
	"goodnighttoall!findlaythanksyou!": 0,
	"h2oklan100k": 0,
	"londonrobbed": 0,
	"lean-toshed": 1,
	"flsuncoastsunrise": 0,
	"cornseed": 1,
	"munzeewoodstockcincinnati": 0,
	"eventzee": 1,
	"egg": 1,
	"munzeevictoriadinner": 0,
	"calf": 1,
	"findlay&#039;ssmashinggreatchristmasevent": 0,
	"seattletourdecure": 0,
	"cherrychocolate2017": 0,
	"dinner": 0,
	"coconutchocolate2017": 0,
	"pinatadrop": 1,
	"darkchocolate2017": 0,
	"redgift": 0,
	"mm3event": 0,
	"chestnutghostwalk": 0,
	"rockysunrise": 0,
	"rosemarybuchananmemorialevent": 0,
	"tourdecuredfw": 0,
	"sebringfloridaco-existgogreen": 0,
	"munzfitlondon": 0,
	"giftburger": 0,
	"cappingthecoast2merrittisland": 0,
	"munzeemeet&amp;greetinashevillenc": 0,
	"faretheewell": 0,
	"christmas2017-florida": 0,
	"robvisitsdeutschland": 0,
	"euroambassador": 0,
	"ridelondon": 0,
	"mhqmunzfit2017": 0,
	"gogreenphoenix,tourdecureandzeetour": 0,
	"nzsouthislandmeetmhq": 0,
	"meetskylar": 0,
	"aussierob": 0,
	"(lgas)salisbury,uk": 0,
	"missmichigan": 0,
	"gw2018munzeemeet&amp;greet": 0,
	"2ndeuropeevent": 0,
	"unicorn": 0,
	"cyclopsvirtualhost": 0,
	"mermaidhost": 1,
	"fairyhost": 1,
	"dragonhost": 1,
	"social": 0,
	"faunhost": 1,
	"firepouchcreaturehost": 1,
	"pegasushost": 0,
	"eventindicator": 1,
	"cyclopshost": "D",
	"cherubvirtualhost": 0,
	"cherubhost": "D",
	"unicornhost": 1,
	"waterpouchcreaturehost": 1,
	"yetihost": 1,
	"leprechaunhost": 1,
	"hydrahost": 1,
	"theprankster": 0,
	"motel": 1,
	"timeshare": 1,
	"personal": "X",
	"aquamarine": 1,
	"hotel": 1,
	"barn": 1,
	"normal": 1,
	"trail": 1,
	"creepybag": 1,
	"treasureship": 1,
	"backtoschool2016": 1,
	"summerseasonalmunzee": 1,
	"ghostship": 0,
	"cargoship": 1,
	"jewelthiefnomad": 1,
	"lion": 1,
	"warriornomad": 1,
	"tulimber": 1,
	"pinatalollipop": 1,
	"mime2018": 1,
	"mayflowers": 1,
	"plow": 1,
	"dinosaur": 1,
	"texasindependence": 1,
	"gardenshed": 1,
	"gift2014": 1
}
reloadMunzeeList()
useful.reqnames = {
	cap_p: "Cap", 
	dep_p: "Dep", 
	con_p: "CapOn",
	tot_p: "Total",
	cap: "#Cap",
	dep: "#Dep",
	gam_cap: "#GamCap",
	phy_dep: "#PhyDep",
	myth_cap: "#MythCap",
	phy_cap: "#PhyCap",
	c_days: "#CapDays",
	d_days: "#DepDays",
	jewel_dep: "#JewelDep",
	clan_dep: "#WeaponDep",
	tot_xcon: "Cap&Dep",
	cap_xsoc: "#Cap",
	dep_xsoc: "#Dep",
	mys_cap: "#MystCap",
	mothot_cap: "#M/HotelCap",
	mys_dep: "#MystDep",
	phyjew_cap: "#PhyJewelCap",
	phyevo_cap: "#PhyEvoCap",
	prem_cap: "#PremCap",
	phyjew_dep: "#PhyJewelDep",
	phyevo_dep: "#PhyEvoDep",
	prem_dep: "#PremDep",
	mothot_dep: "#M/HotelDep",

	// myth_cap: 20,
	tempvirt_cap: '#TmpVrtCap',
	event_cap: '#EvntIndCap',
	rmyth_cap: '#RtMythCap',
	zeeqrew_replace: '#ZQrewRplce',
	virtevo_cap: '#VrtEvoCap',
	surprise_cap: '#SurpCap',
	scatter_cap: '#ScatCap',
	scatter_dep: '#ScatDep',
	scattered_cap: '#ScttrdCap',
	prize_cap: '#PrzWhlCap'
};
useful.reqimgs = {
	cap_p: ["Points","https://munzee.global.ssl.fastly.net/images/v4pins/captured_physical.png"], 
	dep_p: ["Points","https://munzee.global.ssl.fastly.net/images/v4pins/owned_physical.png"], 
	con_p: ["Points","https://preview.ibb.co/dLO6GL/capon.png"], 
	tot_p: ["Points","https://munzee.global.ssl.fastly.net/images/v4pins/munzee.png"],
	myth_cap: ["Cap","https://munzee.global.ssl.fastly.net/images/v4pins/theunicorn.png"],
	clan_dep: ["Dep","https://munzee.global.ssl.fastly.net/images/v4pins/longsword.png"],
	tempvirt_cap: ["Cap","https://munzee.global.ssl.fastly.net/images/v4pins/temporaryvirtual.png"],
	event_cap: ["Cap","https://munzee.global.ssl.fastly.net/images/v4pins/eventindicator.png"],
	rmyth_cap: ["Cap","https://munzee.global.ssl.fastly.net/images/v4pins/retiredunicorn.png"],
	zeeqrew_replace: ["Replace","https://munzee.global.ssl.fastly.net/images/v4pins/maintenance.png"],
	virtevo_cap: ["Cap","https://munzee.global.ssl.fastly.net/images/v4pins/evolution.png"],
	surprise_cap: ["Cap","https://munzee.global.ssl.fastly.net/images/v4pins/surprise.png"],
	scatter_cap: ["Cap","https://munzee.global.ssl.fastly.net/images/v4pins/scatter.png"],
	scatter_dep: ["Dep","https://munzee.global.ssl.fastly.net/images/v4pins/scatter.png"],
	scattered_cap: ["Cap","https://munzee.global.ssl.fastly.net/images/v4pins/scattered.png"],
	prize_cap: ["Cap","https://munzee.global.ssl.fastly.net/images/v4pins/prizewheel.png"]
};
useful.reqdescs = {
	cap_p: "Capture Points",
	dep_p: "Deploy Points",
	con_p: "Capture-on Points",
	tot_p: "Total Points",
	cap: "Number of Captures",
	dep: "Number of Deploys",
	gam_cap: "Number of Gaming Munzee Captures",
	phy_dep: "Number of Physical Deploys",
	myth_cap: "Number of Myth Captures",
	phy_cap: "Number of Physical Captures",
	c_days: "Capture Days",
	d_days: "Deploy Days",
	jewel_dep: "Number of Jewel Deploys",
	clan_dep: "Number of Weapon Deploys",
	tot_xcon: "Capture and Deploy Points",
	cap_xsoc: "Number of Captures, excluding Socials",
	dep_xsoc: "Number of Deploys, excluding Socials",
	mys_cap: "Number of Mystery Captures",
	mothot_cap: "Number of Motel/Hotel (Inc. Rooms) Captures",
	mys_dep: "Number of Mystery Deploys",
	phyjew_cap: "Number of Physical Jewel Captures",
	phyevo_cap: "Number of Physical Evolution Captures",
	prem_cap: "Number of Premium Captures",
	phyjew_dep: "Number of Physical Jewel Deploys",
	phyevo_dep: "Number of Physical Evolution Deploys",
	prem_dep: "Number of Premium Deploys",
	mothot_dep: "Number of Motel/Hotal (Inc. Rooms) Deploys",


	tempvirt_cap: 'Number of Temporary Virtual Captures',
	event_cap: 'Number of Event Indicator Captures',
	rmyth_cap: 'Number of RetireMyth Captures',
	zeeqrew_replace: 'Number of ZeeQRew Replaces',
	virtevo_cap: 'Number of Virtual Evolution Captures',
	surprise_cap: 'Number of Surprise Captures',
	scatter_cap: 'Number of Scatter Captures',
	scatter_dep: 'Number of Scatter Deploys',
	scattered_cap: 'Number of Scattered Captures',
	prize_cap: 'Number of Prize Wheel Captures'
}
useful.reqfrom = {
	cap_p: ['d','capture_points','capture'],
	dep_p: ['d','deploy_points','deploy'],
	con_p: ['d','capture_on_points','capture_on'],
	tot_p: ['d','total_points','total'], 
	tot_xcon: ['d','capture_points+deploy_points','capture+deploy'],
	cap: ['s','number of captures'], 
	cap_xsoc: ['s','number of captures'],
	gam_cap: ['s','number of gaming captures'], 
	phy_dep: ['s','number of physical deploys'], 
	dep: ['s','number of deploys'], 
	dep_xsoc: ['s','number of deploys'],
	myth_cap: ['s','number of myth caps'],
	phy_cap: ['s','number of physical captures'],
	c_days: ['s','number of capture days'],
	d_days: ['s','number of deploy days'],
	jewel_dep: ['s','number of jewel deploys'],
	clan_dep: ['s','number of weapon deploys'],
	//Sept18"number of jewel deploys","number of destination deploys","number of mystery deploys","number of evolution deploys","number of jewel caps","number of evolution caps","number of destination caps","number of mystery caps"
	mys_cap: ['s','number of mystery caps'],
	mothot_cap: ['s','number of destination caps'],
	mys_dep: ['s','number of mystery deploys'],
	phyjew_cap: ['s','number of jewel caps'],
	phyevo_cap: ['s','number of evolution caps'],
	prem_cap: ['s','number of premium caps'],
	phyjew_dep: ['s','number of jewel deploys'],
	phyevo_dep: ['s','number of evolution deploys'],
	prem_dep: ['s','number of premium deploys'],
	mothot_dep: ['s','number of destination deploys'],


	tempvirt_cap: ['s','number of temp virt caps'],
	event_cap: ['s','number of event pin caps'],
	rmyth_cap: ['s','number of retire myth caps'],
	zeeqrew_replace: ['s','number of replaced munzees'],
	virtevo_cap: ['s','number of evolution caps'],
	surprise_cap: ['s','number of surprise caps'],
	scatter_cap: ['s','number of scatter caps'],
	scatter_dep: ['s','number of scatter deploys'],
	scattered_cap: ['s','number of scattered caps'],
	prize_cap: ['s','number of prize wheel caps']
}
// USEFUL INFO!

global.a = 0;

global.timezone, global.hqT, global.doffset = 0, global.gmtOffset = 0;
global.gTZ = async function(){
	//Get Timezone
	timezone = JSON.parse(await rp('https://api.timezonedb.com/v2/get-time-zone?key=S0T3SKWNPU73&format=json&by=position&lat=33.214561&lng=-96.614456'));
	doffset = (timezone.gmtOffset * 1000) + ((new Date()).getTimezoneOffset() * 60000);
	gmtOffset = timezone.gmtOffset * 1000;
	console.log('gottimezoneinfo');
}
gTZ();
global.hqStr = function(){
	var tm = hqT();
	let month = String(tm.getMonth() + 1);
	let date = String(tm.getDate());
	if(month.length == 1){
		month = "0" + month;
	}
	if(date.length == 1){
		date = "0" + date;
	}
	return `${tm.getFullYear()}-${month}-${date}`;
}
global.hqT = function(){return new Date(Date.now() + doffset);};
global.dayStart = function(){
	var xxx = new Date(Date.now())
	xxx.setHours(0,-new Date(Date.now()).getTimezoneOffset(),0,-gmtOffset);
	if(xxx.valueOf() > new Date(Date.now()).valueOf()) xxx = new Date(xxx.valueOf() - (24 * 60 * 60 * 1000));
	return xxx;
}

global.getUName = async function(id, doc){ // Get Username of User
	var auth = doc ? doc.access_token : await getAT();
	var options = {
		method: 'POST',
		uri: 'https://api.munzee.com/user/',
		headers: {
			'Authorization': auth, 
		},
		formData: {
			data: `{"user_id":${id}}`, 
		}
	};
	var body = await rp(options);
	var data = JSON.parse(body).data;
	return data.username;
}

global.getAT = async function(id, expired){ //Get User Access Token
    var doc, xxx;
    if(id && typeof id == "number"){
        xxx = access.doc(getUName(id));
    } else if(id){
        xxx = access.doc(getUName(id));
    } else {
        xxx = access.doc('WriteAndMane');
	}
	doc = SOP(await xxx.get());
	if(!doc) return false;
	var newdoc = doc;
    if((Date.now() + 60000) / 1000 > newdoc.expires || expired){
		try {
			var options = {
				method: 'POST',
				uri: 'https://api.munzee.com/oauth/login',
				formData: {
					'client_id': config.client_id,
					'client_secret': config.client_secret,
					'grant_type': 'refresh_token',
					'refresh_token': doc.refresh_token, 
					'redirect_uri': config.redirect_uri
				}
			};
			Object.assign(newdoc, JSON.parse(await rp(options)).data.token);
			newdoc.username = await getUName(newdoc.user_id, newdoc);
			await xxx.set(newdoc);
		} catch(e) {
			// console.log(doc.username);
			if(e.toString().includes('The refresh token is invalid.')){
				console.log('REMOVED', e.toString());
				await xxx.remove();
			}
		}
    }
    return newdoc.access_token;
}

global.getUName = async function(id, doc){
	var auth = doc ? doc.token.access_token : await getAT();
	var options = {
		method: 'POST',
		uri: 'https://api.munzee.com/user/',
		headers: {
			'Authorization': auth, 
		},
		formData: {
			data: `{"user_id":${id}}`, 
		}
	};
	var body = await rp(options);
	var data = JSON.parse(body).data;
	return data.username;
}

global.mR = async function(page, data, id, expired){ // Munzee Request
	var auth = id ? id : await getAT();
	if(!auth) console.log('No auth?');
	var options = {
		method: 'POST',
		uri: encodeURI('https://api.munzee.com/' + page),
		headers: {
			Authorization: encodeURI(auth),
		},
		formData: {
			data: JSON.stringify(data), 
		}
	};
	// console.log("OPTIONS:", options);
	try {
		var raw = await rp(options)
		var body = JSON.parse(raw).data;
		return body;
	} catch(e) {
		// console.log('Unexpected error occurred', e);
		//discordMsg('000', await access.where('access_token', '==', auth).get());
		discordMsg('111',e);
		var uid = SOP(await (access.where('access_token', '==', auth)).get()).user_id;
		if(!expired) {
			return await mR(page, data, await getAT(uid), true);
		} else {
			return Error('Unable to get data');
		}
	}
}
global.postTS = async function(){
	rp({
		method: 'POST',
		uri: 'http://ptsv2.com/t/1bgtz-1536163241/post',
		formData: {
			username: 'CuppaZee Notification',
			content: JSON.stringify(arguments)
		}
	})
		.catch(console.log);
}
global.discordMsg = async function(){
	let ret = JSON.stringify(arguments);
	for(var i = 0;i < ret.length / 1900 && i < 10;i++){
		rp({
			method: 'POST',
			uri: 'https://ptb.discordapp.com/api/webhooks/476077608078147598/CoMntIEMXfuB_CrGfH6BOfmVvOdnCsE336JoZ5a-_jPVpcjlX6JlG3B4x_d6ij_ZLiBC',
			formData: {
				username: 'CuppaZee Notification',
				content: i + "```" + ret.slice(i*1900,(i+1)*1900) + "```"
			}
		})
			.catch(console.log);
	}
	
}
global.cleanCapDep = function(a){
	return a.map(i=>{
		return i.icon.replace(/https:\/\/munzee.global.ssl.fastly.net\/images\/(?:v4)?pins\/(?:svg\/)?(.+)\..+/,'$1')
	});
}
global.betterZeeQRew = async function(u){
	var zq = await zeeQRew(u);
	var ip = await rp(`https://munzee.com/m/${u}`);
	var out = {
		handle: '',
		premium: false,
		level: 0,
		points: 0,
		physical_captures: 0,
		physical_deploys: 0
	}
	out.handle = ip.match(/<div class="avatar-username"><a href="\/m\/[^\/]+\/">([^<]+)<\/a><\/div>/)[1];
	if(ip.includes('class="fa fa-star"')) out.premium = true;
	out.points = Number(ip.match(/<a href="https:\/\/statzee.munzee.com\/player\/points\/[^"]+">([0-9,]+)<\/span>/)[1].replace(/,/g,''));
	out.level = Number(ip.match(/<a href="#" data-toggle="tooltip" title="Current Level" class="user-rank tooltip-holder"><span class="badge badge-success">([0-9,]+)<\/span><\/a>/)[1].replace(/,/g,''));
	var virts = [];
	for(var i = 0;i < Object.keys(zq.c).length;i++){
		let key = Object.keys(zq.c)[i];
		let obj = zq.c[key];
		if(useful.phyVirt[key] == 1) out.physical_captures += obj;
		// MYTH STUFF HERE
	}
	for(var i = 0;i < Object.keys(zq.d).length;i++){
		let key = Object.keys(zq.d)[i];
		let obj = zq.d[key];
		if(useful.phyVirt[key] == 1) out.physical_deploys += obj;
		if(useful.phyVirt[key] == 1) virts.push(`${key}:${obj}`);
	}
	discordMsg(virts);
	return out;
}
global.zeeQRew = async function(u){
	//uSER, pAGE
    var uri = `https://statzee.munzee.com/player/captures/${u}`
	var j = rp.jar();
    var url = "https://statzee.munzee.com";
    var cookie = rp.cookie(config.userLogin);
    j.setCookie(cookie, url);
    var caps = await rp({uri, jar:j});
    var deps = await rp({uri: uri.replace('captures','deploys'), jar:j});
    var out = {c: {}, d: {}};
	caps = caps.replace(/\n/g,'').replace(/[^]+<div class="page-header summary"> <h3>Captures By Type<\/h3> <\/div> <table class="table table-hover" style="margin-top: 40px;"> <thead> <tr> <th><\/th> <th class="hidden-xs">Capture Type<\/th> <th>Captures<\/th> <th>Average Points<\/th> <th>Total Points<\/th> <th class="hidden-xs">Percentage<\/th> <\/tr> <\/thead> <tbody>([^]+)<\/tbody> <\/table> <\/div> <\/div> <\/div> <\/div>[^]+/,'$1').split('</tr> <tr>').map(i=>i.replace(/<\/?tr>/g,''));
	deps = deps.replace(/\n/g,'').replace(/[^]+<div class="page-header summary"> <h3>Deployed Munzees By Type<\/h3> <\/div> <table class="table table-hover" style="margin-top: 40px;"> <thead> <tr> <th><\/th> <th class="hidden-xs">Munzee Type<\/th> <th>Munzees<\/th> <th>Captures On<\/th> <th>Deploy Points<\/th> <th>Cap On Points<\/th> <th>Combined Points<\/th> <th class="hidden-xs">Percentage<\/th> <\/tr> <\/thead> <tbody>([^]+)<\/tbody> <\/table> <\/div> <\/div> <\/div> <\/div>[^]+/,'$1').split('</tr> <tr>').map(i=>i.replace(/<\/?tr>/g,''));
	for(var i = 0;i < caps.length-1;i++){
		let cap = caps[i];
		out.c[cap.match(/img src="([^"]+)"/)[1].replace(/https:\/\/munzee.global.ssl.fastly.net\/images\/pins\/(?:svg\/)?([^\.]+)\..+/,'$1')] = Number(cap.match(/<td class="hidden-xs"><i>[^<]+<\/i><\/td> ?<td>([^<]+)<\/td>/)[1].replace(/,/g,''));
	}
    for(var i = 0;i < deps.length-1;i++){
		let dep = deps[i];
		out.d[dep.match(/img src="([^"]+)"/)[1].replace(/https:\/\/munzee.global.ssl.fastly.net\/images\/pins\/(?:svg\/)?([^\.]+)\..+/,'$1')] = Number(dep.match(/<td class="hidden-xs"><i>[^<]+<\/i><\/td> ?<td>([^<]+)<\/td>/)[1].replace(/,/g,''));
	}
	return out;
}
global.newPlayerCapDep = async function(u,p,dep,special,debug){
    try {
	//uSER, pAGE
	var uri = `https://www.munzee.com/m/${u}/${dep ? 'deploys' : 'captures'}/${special ? "specials/"+special+"/" : ''}${p}/`
	var j = rp.jar();
	var url = "https://www.munzee.com";
	var cookie = rp.cookie(config.userLogin);
	j.setCookie(cookie, url);
	var caps = await rp({uri, jar:j}); //,jar:j
	    
	    if(debug)discordMsg(uru);
	    var out = [];
	    if(!caps.includes('munzee-holder')) discordMsg('err');
		caps = caps.replace(/\n/g,'').replace(/[^]+<div id="munzee-holder">([^]+)<\/div> <ul class="pager">[^]+/,'$1').split('<section>').map(i=>i.replace('</section>',''));
		if(debug)postTS(caps);
	    for(var i = 0;i < caps.length-1;i++){
			let cap = caps[i+1];
			out[i] = {};
			if(!dep) out[i].captured_at = cap.match(/data-captured-at="([^"]+)"/)[1];
			out[i].deployed_at = cap.match(/data-deployed-at="([^"]+)"/)[1];
			out[i].icon = cap.match(/class="pin(?: hidden-xs)?" src="([^"]+)">/)[1];
			out[i].points = cap.match(/<div class="number">([^<]+)<\/div>/)[1];
			out[i].name = cap.match(/<a class="munzee-id" href="([^"]+)">([^<]+)<\/a>/)[2];
			out[i].url = "https://www.munzee.com" + cap.match(/<a class="munzee-id" href="([^"]+)">([^<]+)<\/a>/)[1];
			out[i].type = useful.types[out[i].icon.replace(/https:\/\/munzee.global.ssl.fastly.net\/images\/pins\/(?:svg\/)?(.+)\..+/,'$1')];
			if(typeof out[i].type == 'function'){out[i].type = out[i].type(out[i]);}
			if(!out[i].type) out[i].type = -1;
			out[i].capture_type_id = out[i].type;
		}
		//if(u == "sohcah"){discordMsg('Sohcap',out)};
		return out;
	} catch(e){
		if(true) { // e.toString().includes('404')
			return "PlayerBanned";
		} else {
			discordMsg('ShoutButtercup',e.toString());
		}
	}
}
global.getPlayerCapAmount = async function(player){
	var begin = Date.now();
	var uri = `https://statzee.munzee.com/player/captures/${player}`
	var j = rp.jar();
	var url = "https://statzee.munzee.com";
	var cookie = rp.cookie(config.userLogin);
	j.setCookie(cookie, url);
	var mincaps = Number((await rp({uri, jar:j})).match(new RegExp(`data\-original\-title="[^,]+, ${hqStr()}">[^0-9<]*(?:<a[^>]*>)?[^0-9<]*([0-9]+)`))[1]);
	return mincaps;
}
global.GPA = async function(player){
	// GetPlayerActivity
	try {
		//discordMsg(player, (await mR('user/',{username:player}))["user_id"]);
		/* Commented out due to new system =)
		if(await (access.doc(player)).get()._fieldsProto){
			//Authenticated
			//discordMsg(player, await (access.doc(player)).get());
			var token = getAT(player);
			var i = await mR('statzee/player/day',{"day":hqStr()});
			return {dep: i.deploys, cap: i.captures};
		} else {
		Commented out due to new system =)	
		*/
			//Non-Authed
			/*UserID replaced by Player Name
			var useridbefore = (await mR('user/',{username:player}));
			if(!useridbefore){
				return {cap: [], dep: []};
			}useridbefore["user_id"]
			*/
			var token = getAT(), ds = dayStart(), cap = [], dep = [], i, uid = player,tempucad = [];
			if(!usercache[uid] || !usercache[uid].cap || !usercache[uid].dep){
				usercache[uid] = {cap: [], dep: []};
				ucad[uid] = [];
			}
			if((usercache[uid].cap.length > 0 && (new Date(usercache[uid].cap[0].captured_at)).valueOf() < ds.valueOf()) || ((usercache[uid].dep.length > 0 && (new Date(usercache[uid].dep[0].captured_at)).valueOf() < ds.valueOf()))){
				usercache[uid] = {cap: [], dep: []};
				ucad[uid] = [];
			}
			var caps = usercache[uid].cap.length;
			var mincaps = 0;
			// var mincaps = await getPlayerCapAmount(player);
			// discordMsg(`data\-original\-title="[^,]+, ${hqStr()}">[^0-9<]*(?:<a[^>]*>)?[^0-9<]*([0-9]+)`)
			// var mincaps = Number((await rp({uri, jar:j})).match(new RegExp(`data\-original\-title="[^,]+, ${hqStr()}">[^0-9<]*(?:<a[^>]*>)?[^0-9<]*([0-9]+)`))[1]);
			// discordMsg(mincaps,player);

			for(var sp = 0;sp < useful.specials.length;sp++){
				for(i = 0;i < 100;i++){
					//let dt = await mR('user/captures',{user_id:uid, page: i});
					let dt = await newPlayerCapDep(player,i,false,useful.specials[sp]);
					if(dt=="PlayerBanned") return {banned:true,cap:[],dep:[]};
					//if(i == 0 && player == 'sohcah') discordMsg('Capture',dt);
					// if(uid == 125932){
					// 	postTS('GunnerSteve DT', i, dt);
					// }
					//discordMsg('capdt', dt[0]);
					if(dt.length==0) break;
					for(let j = 0;j < dt.length;j++){
						if((new Date(dt[j].captured_at)).valueOf() >= ds.valueOf() && !ucad[uid].includes(AID(uid,dt[j],'C'))){ // && !(usercache[uid].cap.length > 0 && dt[j].captured_at == usercache[uid].cap[usercache[uid].cap.length - 1].captured_at && dt[j].munzee_id == usercache[uid].cap[usercache[uid].cap.length - 1].munzee_id)
							if(!tempucad.includes(AID(uid,dt[j],'C'))){
								cap.push(dt[j]);
								tempucad.push(AID(uid,dt[j],'C'));
								caps=usercache[uid].cap.length + cap.length;
							}
						} else if(true || caps >= mincaps) {
							//discordMsg('EndedGettingCaptures', player, dt[j], ds.valueOf())
							i = 10000;
							break;
						} else {
							break;
						}
					}
				}
			}
			for(i = 0;i < 100;i++){
				//let dt = (await mR('user/deploys',{user_id:uid, page: i})).munzees;
				let dt = await newPlayerCapDep(player,i,true);
				//discordMsg('depdt', dt[0]);
				for(let j = 0;j < dt.length;j++){
					if((new Date(dt[j].deployed_at)).valueOf() >= ds.valueOf() && !ucad[uid].includes(AID(uid,dt[j],'D'))){ // && !(usercache[uid].dep.length > 0 && dt[j].deployed_at == usercache[uid].dep[usercache[uid].dep.length - 1].deployed_at && dt[j].munzee_id == usercache[uid].dep[usercache[uid].dep.length - 1].munzee_id)
						dep.push(dt[j]);
						tempucad.push(AID(uid,dt[j],'D'));
					} else {
						i = 10000;
						break;
					}
				}
			}
			for(i = 0;i < cap.length;i++){
				usercache[uid].cap.push(cap[i]);
			}
			for(i = 0;i < tempucad.length;i++){
				ucad[uid].push(tempucad[i]);
			}
			for(i = 0;i < dep.length;i++){
				usercache[uid].dep.push(dep[i]);
			}
			return {dep: usercache[uid].dep, cap: usercache[uid].cap}
		/*
		Commented out due to new system =)
		}
		Commented out due to new system =)	
		*/
	} catch(e) {
		discordMsg(e.toString());
	}
	
	
}
global.SOP = function(s){
	// Store OutPut
	var fields = s._fieldsProto;
	var ret = {};
	Object.keys(fields).map(ii=>{
		i = fields[ii];
		var val = i[i.valueType];
		if(i.valueType == "integerValue"){
			ret[ii] = Number(val);
		} else {
			ret[ii] = val;
		}
	});
	return ret;
};
global.AID = function(uid, x,w){
	// ActivityID
	return w + (w == 'C' ? "|" + x.captured_at : '') + "|" + x.name + "|" + uid + "|" + x.icon + "|" + x.deployed_at + "|" + x.points;
}
global.getClanData = async function(clanid){
	clanreload[clanid] = Date.now();
	var munzees = Object.assign({},useful.munzees);
	var months = ['January','Febuary','March','April','May','June','July','August','September','October','November','December','January'];
	
	var clan = [],timenow = hqT(), token = await getAT(),requ = useful.requirements[timenow.getMonth()],reqls = [],allreq = ['cap_p','dep_p','con_p','tot_p']; //,'con_p'
	if(useful.requirements[timenow.getMonth() + 1] && clanid == -1) requ = useful.requirements[timenow.getMonth() + 1];
	Object.keys(requ.user[4]).map((item)=>{reqls.push({t: "user",i: item})});
	Object.keys(requ.clan[4]).map((item)=>{reqls.push({t: "clan",i: item})});
	reqls.map((item)=>{if(!allreq.includes(item.i))allreq.push(item.i)});
	if(clanid <= 0){
		// Requirements \/
		var r = {name: 'Requirements', id: 0, tables: {}};
		var tables = [
			`Main`,
			`Rewards`,
			`Next Month`
		];
		for(var i = 0;i < 3;i++){
			let table;
			if(i == 1){
				// Rewards
				table = [[{val: 'Sorry, Rewards aren\'t shown yet'}]];
			} else {
				if(useful.requirements[(timenow.getMonth()+(i/2)) % 12]){
					let rqls = [];
					let req = useful.requirements[timenow.getMonth()+(i/2) % 12];
					table = [[{val: req.name,lvl:-3},{val: 'Clan Total',colspan: Object.keys(req.clan[4]).length,lvl:-3},{val: 'User',colspan: Object.keys(req.user[4]).length,lvl:-3}],[{val: 'Level',lvl:-3, reqimg: 'none'}],[{val: 'Level 1', lvl: 1}],[{val: 'Level 2', lvl: 2}],[{val: 'Level 3', lvl: 3}],[{val: 'Level 4', lvl: 4}],[{val: 'Level 5', lvl: 5}]];
					// table[0].push({val: 'Clan', desc: 'Clan Total Requirements'});
					for(var rq of Object.keys(req.clan[4])){
						table[1].push({val: '' + useful.reqnames[rq], reqval: useful.reqimgs[rq][0], desc: 'Clan ' + useful.reqdescs[rq],lvl:-3, reqimg: useful.reqimgs[rq][1]});
						rqls.push('clan_' + rq);
					}
					// table[0].push({val: 'User', desc: 'User Requirements'});
					for(var rq of Object.keys(req.user[4])){
						table[1].push({val: '' + useful.reqnames[rq], reqval: useful.reqimgs[rq][0], desc: 'User ' + useful.reqdescs[rq],lvl:-3, reqimg: useful.reqimgs[rq][1]});
						rqls.push('user_' + rq);
					}

					for(var l = 0;l < 5;l++){
						// table[l+1].push({val: '', lvl: -1});
						for(var rq in req.clan[l]){
							table[l+2][rqls.indexOf('clan_'+rq)+1] = {val: req.clan[l][rq] || '---', bold: !!req.clan[l][rq], lvl: l+1};
						}
						// table[l+1].push({val: '', lvl: -1});
						for(var rq in req.user[l]){
							table[l+2][rqls.indexOf('user_'+rq)+1] = {val: req.user[l][rq] || '---', bold: !!req.user[l][rq], lvl: l+1};
						}
					}
				}
			}
			r.tables[tables[i]] = table;
		}
		r.timestamp = Date.now();
		clanreload[clanid] = false;
		return r;
		// Requirements /\
	}
	// Clan Stats
	var basicData = {};
	clan[0] = await mR('clan/',{"clan_id":clanid},token);
	clan[1] = await mR('clan/stats',{"clan_id":clanid},token);
	if(clan[1] && clanid == 1349 && timenow.getDate() == 4){
		discordMsg('CLANDATA', Object.keys(clan[1]));
	}
	if(timenow.getDate() < 3){
		var ret = {name: clan[0].details.name,
			id: clanid, 
			tables: {
				'Main':[
					[
						{val: ''},{val: `Members (${clan[0].users.length}) - C&D Indicators Not Available`, bold: true}
					]
				]
			},
			timestamp: Date.now()
		}
		for(var i = 0;i < clan[0].users.length;i++){
			ret.tables.Main[i+1] = [{val: ''},{val: clan[0].users[i].username}];
		}
		clanreload[clanid] = false;
		return ret;
	}
	
	// Basic Data \/
	for(var i = 0;i < clan[0].users.length;i++){
		basicData[clan[0].users[i].username] = {};
		for(let j = 0;j < allreq.length;j++){
			let rqfrm = useful.reqfrom[allreq[j]];
			if(rqfrm[0] == "d"){
				basicData[clan[0].users[i].username][allreq[j]] = clan[0].users[i][rqfrm[1]] || 0;
			} else {
				if(clan[1] && clan[1][rqfrm[1]]){
					basicData[clan[0].users[i].username][allreq[j]] = clan[1][rqfrm[1]][clan[0].users[i].username] || 0;
				} else {
					basicData[clan[0].users[i].username][allreq[j]] = 0;
				}
			}
		}
		let ud = await GPA(clan[0].users[i].username);
		function inc(req,amount){
			if(allreq.includes(req)) basicData[clan[0].users[i].username][req] += amount || 1;
		}
		if(ud.banned){
			basicData[clan[0].users[i].username].banned = true;
		}
		for(let j = 0;j < ud.cap.length;j++){
			let cap = ud.cap[j];
			let md = useful.munzees[Number(cap.capture_type_id).toString()];
			inc('cap');
			if(md){
				// May 18
				if(md.gaming) inc('gam_cap');
	
				// June 18
				if(md.physical) inc('phy_cap');
				if(md.physical && (md.mythhost || md.alternahost || md.pouchhost)) inc('phy_cap');
				if(md.myth || md.alterna) inc('myth_cap');
	
				// July 18
				basicData[clan[0].users[i].username].cap_day = true;
	
				// August 18
				if(md.id != "social") inc('cap_xsoc');	

				// September 18
				if(md.id == 'mystery') inc('mys_cap');
				if(['motel','hotel','motel-room','hotel-room'].includes(md.id)) inc('mothot_cap');
				if(md.physical && md.jewel) inc('phyjew_cap');
				if(md.physical && md.evolution) inc('phyevo_cap');
				if(md.id == 'premium') inc('prem_cap');

				// October 18
				if(md.id == "temp-virtual") inc('tempvirt_cap');
				if(md.id == "event-indicator") inc('event_cap');
				if(md.retiremyth) inc('rmyth_cap');
				if(md.virtual && md.evolution) inc('virtevo_cap');
				if(md.id == "surprise") inc('surprise_cap');
				if(md.id == "scatter") inc('scatter_cap');
				if(md.id == "scattered") inc('scattered_cap');
				if(md.id == "prize-wheel") inc('prize_cap');
			}

			
		}
		for(let j = 0;j < ud.dep.length;j++){
			let dep = ud.dep[j];
			let md = useful.munzees[Number(dep.capture_type_id).toString()];
			inc('dep');
			if(md){
				// June 18
				if(md.physical) inc('phy_dep');
	
				// July 18
				if(md.clan) inc('clan_dep');
				if(md.jewel) inc('jewel_dep');

                basicData[clan[0].users[i].username].dep_day = true;
	
				// August 18
				if(md.id != "social") inc('dep_xsoc');

				// September 18
				if(md.id == 'mystery') inc('mys_dep');
				if(md.physical && md.jewel) inc('phyjew_dep');
				if(md.physical && md.evolution) inc('phyevo_dep');
				if(md.id == 'premium') inc('prem_dep');
				if(['motel','hotel','motel-room','hotel-room'].includes(md.id)) inc('mothot_dep');

				// October 18
				if(md.id == "scatter") inc('scatter_dep');
			}

			
		}
	}
	// Basic Data /\

	// To Tables \/

    var r = {};
    r.name = clan[0].details.name;
    r.rank = clan[0].ranking + "/" + clan[0].number_of_clans;
	r.id = clanid;
    r.tables = {};

    var users = Object.keys(basicData);
    users.sort(function(a,b){
	return basicData[b].tot_p - basicData[a].tot_p;
    });

    var tls = ['Main', '10%', 'Level 1', 'Level 2', 'Level 3', 'Level 4', 'Level 5'];

    for (var tn = 0; tn < 7; tn++) {
	let CDSep = 1; // 1 - No, 2 - Yes
        let table = r.tables[tls[tn]] = [[{ lvl: -1, val: 'Players', reqimg: 'none' }]]; //, { lvl: -1, val: '', desc: 'Captured and Deployed Today?' }
        let un = 0; // User number
        let clanLvl = 5;
        let ul = Object.keys(basicData).length;
        let lrow = table[ul + 1] = [{ lvl: 5, val: 'CLAN TOTAL' }]; //, {lvl: -1}
        for (let rq of allreq) { table[0].push({ lvl: -1, val: useful.reqnames[rq], reqval: useful.reqimgs[rq][0], desc: useful.reqdescs[rq], rawrq: rq, reqimg: useful.reqimgs[rq][1] }) };
        table[0].push({ lvl: -1, val: 'Lvl', desc: 'Level', reqimg: 'none' });
        let clanTotal = {};
        for (let name of users) {
            // For each Person
            un++;
            var userLvl = 5;
            var ud = basicData[name];//Userdata
            var row = table[un] = [{ lvl: 5, val: name, player: true, desc: 'Click to go to ' + name + '\'s Munzee Page' }, { lvl: -1 }];
            for (let rq in ud) {
                // rq = requirement
                let sc = ud[rq]; // Score
                if (rq == "cap_day") { row[CDSep - 1].cap = sc } else if (rq == "dep_day") { row[CDSep - 1].dep = sc } else if (rq == "banned") { row[CDSep - 1].banned = true; row[CDSep - 1].desc = 'Can\'t access ' + name + '\'s Munzee Data'; row[CDSep - 1].player = false; } else {
                    if (tn < 2) {
                        let lvl = 0;
                        if (tn == 0) {
                            for (lvl = 0; lvl < 5 && (requ.user[lvl][rq] || 0) <= sc; lvl++) { }
                            if (!requ.user[4][rq]) lvl = -2;
                        } else {
                            for (lvl = 0; lvl < 5 && Math.max(((requ.clan[lvl][rq] || 0) / 10), requ.user[lvl][rq] || 0) <= sc; lvl++) { }
                            if (!requ.clan[4][rq] && !requ.user[4][rq]) lvl = -2;
                        }
                        row[allreq.indexOf(rq) + CDSep] = { val: sc, lvl: lvl };
                        if (lvl != -2) userLvl = Math.min(userLvl, lvl);
                    } else {
                        let lvl = tn - 2;
                        if (requ.user[lvl][rq]) {
                            row[allreq.indexOf(rq) + CDSep] = { val: Math.max(0, requ.user[lvl][rq] - sc), lvl: requ.user[lvl][rq] > sc ? 0 : lvl + 1 };
                            userLvl = Math.min(requ.user[lvl][rq] > sc ? 0 : lvl + 1, userLvl);
                        } else {
                            row[allreq.indexOf(rq) + CDSep] = { val: 0, lvl: -2 };
                        }
                    }
                    clanTotal[rq] = (clanTotal[rq] || 0) + sc;
                }
			}
			if(Object.keys(requ.user[4]).length == 0) userLvl = "---";
            row[0].lvl = userLvl == "---" ? -2 : userLvl;
            if(CDSep == 2) row[1].lvl = userLvl == "---" ? -2 : userLvl;
            row[row.length] = {lvl: userLvl == "---" ? -2 : userLvl, val: userLvl, bold: userLvl == "---", desc: `${name} is at Level ${userLvl}`};
            if(userLvl != "---") clanLvl = Math.min(clanLvl, userLvl);
        }
        for (let rq of allreq) {
            let sc = clanTotal[rq];
            if (tn < 2) {
                for (lvl = 0; lvl < 5 && (requ.clan[lvl][rq] || 0) <= sc; lvl++) { }
                if (!requ.clan[4][rq]) lvl = -2;
                lrow[allreq.indexOf(rq) + CDSep] = { val: sc, lvl: lvl };
                if (lvl != -2) clanLvl = Math.min(clanLvl, lvl);
            } else {
                let lvl = tn - 2;
                if (requ.clan[lvl][rq]) {
                    lrow[allreq.indexOf(rq) + CDSep] = { val: Math.max(0, requ.clan[lvl][rq] - sc), lvl: requ.clan[lvl][rq] > sc ? 0 : lvl + 1 };
                    clanLvl = Math.min(clanLvl, requ.clan[lvl][rq] > sc ? 0 : lvl + 1);
                } else {
                    lrow[allreq.indexOf(rq) + CDSep] = { val: 0, lvl: -2 };
                }
            }
            //console.log(rq, sc, ul, 'LOL' + (allreq.indexOf(rq) + 2), lrow[allreq.indexOf(rq)+2], table.length);
		}

        lrow[0].lvl = clanLvl;
        if(CDSep == 2) lrow[1].lvl = clanLvl;
        lrow[lrow.length] = {lvl: clanLvl, val: clanLvl, desc: `The clan is at Level ${clanLvl}`};
		
		// RECALCULATE USER LEVELS FOR 10%, AND CLAN LEVEL \/
		if(tn == 1){
			let i = 0;
			for(let name in basicData){
				i++;
				let ur = table[i];//User Row
				for(var j = CDSep;j < ur.length;j++){
					if(j != ur.length - 1){
						if(!requ.clan[4][table[0][j].rawrq] && !requ.user[4][table[0][j].rawrq]){
							table[i][j].lvl = -2;
						} else {
							// Requirement
							let rq = table[0][j].rawrq;
							let ob = table[i][j];
							let cl = table[ul+1][j];
							let lvl = Math.min(cl.lvl, 5);
							for (var xlvl = 0; xlvl < 5 && ((requ.clan[xlvl][rq] || 0) / 10) <= ob.val; xlvl++) { }
							lvl = Math.max(lvl, xlvl);
							if(r.tables[tls[0]][i][j].lvl != -2) lvl = Math.min(r.tables[tls[0]][i][j].lvl, lvl);
							ob.lvl = lvl;
						}
						
					}
				}
				table[i][0].lvl = r.tables[tls[0]][i][0].lvl;
				if(CDSep == 2) table[i][1].lvl = r.tables[tls[0]][i][0].lvl;
				table[i][table[i].length-1] = {val: r.tables[tls[0]][i][0].lvl, lvl: r.tables[tls[0]][i][0].lvl, desc: `${name} is at Level ${r.tables[tls[0]][i][0].lvl}`};
			}
			table[ul+1][0].lvl = r.tables[tls[0]][ul+1][0].lvl;
			if(CDSep == 2) table[ul+1][1].lvl = r.tables[tls[0]][ul+1][0].lvl;
			table[ul+1][table[ul+1].length-1] = {val: r.tables[tls[0]][ul+1][0].lvl, lvl: r.tables[tls[0]][ul+1][0].lvl, desc: `The clan is at Level ${r.tables[tls[0]][ul+1][0].lvl}`};
		}
		// RECALCULATE USER LEVELS FOR 10%, AND CLAN LEVEL /\
    }

    // To Tables /\

	// Return \/
	r.reloadExpires = Date.now() + 300000;
	r.timestamp = Date.now();
	clanreload[clanid] = false;
	//r.basicData = basicData;
    return r;
    // Return /\
}

global.loopFunction = function(){
    a++;
}

global.getClanId = async function(e){
    if(/^\d+$/.test(e.replace('https','http').replace('http://stats.munzee.dk/?',''))) {
        if((await mR('clan/',{"clan_id":Number(e.replace('https','http').replace('http://stats.munzee.dk/?',''))})).users.length > 0) {
            return Number(e.replace('https','http').replace('http://stats.munzee.dk/?',''))
        }
    }
    var x = (await mR('clan/id/',{simple_name: e.replace('http://www.munzee.com/clans/','').replace('https://www.munzee.com/clans/','').replace('http://munzee.com/clans/','').replace('https://munzee.com/clans/','').replace(/[^A-z0-9]/g,'').toLowerCase()})).clan_id;
    if(x != 0){
        return x;
    } else {
        var clanpage = (await rp('https://www.munzee.com/clans')).toLowerCase().replace(/[^a-z0-9<>\\\/]/g,'');
        var regexStatement = new RegExp('<ahref/clans/([^/]+)/>' + e.replace('http://www.munzee.com/clans/','').replace('https://www.munzee.com/clans/','').replace('http://munzee.com/clans/','').replace('https://munzee.com/clans/','').replace(/[^A-z0-9]/g,'').toLowerCase() + '</a>')
        var matching = clanpage.match(regexStatement);
        if(matching){
            x = (await mR('clan/id/',{simple_name: matching[1]})).clan_id;
            return x;
        } else {
            matching = null;
            var clanpage = (await rp('https://www.munzee.com/clans')).toLowerCase().replace(/[^a-z0-9<>\\\/]/g,'');
            var regexStatement = new RegExp('<ahref/clans/([^/]+)/>the' + e.replace('http://www.munzee.com/clans/','').replace('https://www.munzee.com/clans/','').replace('http://munzee.com/clans/','').replace('https://munzee.com/clans/','').replace(/[^A-z0-9]/g,'').toLowerCase() + '</a>')
            matching = clanpage.match(regexStatement);
            if(matching){
                x = (await mR('clan/id/',{simple_name: matching[1]})).clan_id;
                return x;
            } else {
                matching = null;
                var clanpage = (await rp('https://www.munzee.com/clans')).toLowerCase().replace(/[^a-z0-9<>\\\/]/g,'');
                var regexStatement = new RegExp('<ahref/clans/([^/]+)/>' + e.replace('http://www.munzee.com/clans/','').replace('https://www.munzee.com/clans/','').replace('http://munzee.com/clans/','').replace('https://munzee.com/clans/','').replace(/[^A-z0-9]/g,'').toLowerCase() + 'clan</a>')
                matching = clanpage.match(regexStatement);
                if(matching){
                    x = (await mR('clan/id/',{simple_name: matching[1]})).clan_id;
                    return x;
                } else {
                    matching = null;
                    var clanpage = (await rp('https://www.munzee.com/clans')).toLowerCase().replace(/[^a-z0-9<>\\\/]/g,'');
                    var regexStatement = new RegExp('<ahref/clans/([^/]+)/>the' + e.replace('http://www.munzee.com/clans/','').replace('https://www.munzee.com/clans/','').replace('http://munzee.com/clans/','').replace('https://munzee.com/clans/','').replace(/[^A-z0-9]/g,'').toLowerCase() + 'clan</a>')
                    matching = clanpage.match(regexStatement);
                    if(matching){
                        x = (await mR('clan/id/',{simple_name: matching[1]})).clan_id;
                        return x;
                    } else {
                        console.log('CLAN NOT AVAILABLE', e);
                        return 0;
                    }
                }
            }
        }
    }
}
/*global.htmlClan = function(data,t){
	if(!data || !data.name) return discordMsg(data)
	var output = '<!DOCTYPE html><html><head><style>[lvl="0"] {background-color: #FF0000;}[lvl="1"] {    background-color: #EF6500;}[lvl="2"] {    background-color: #FA9102;}[lvl="3"] {    background-color: #FCD302;}[lvl="4"] {    background-color: #BFE913;}[lvl="5"] {    background-color:#55F40B;}[lvl="-2"] {background-color: #d3ffbe;}td,th {border: 1px solid black; white-space:nowrap;}.center, td:not(:first-child), th:not(:first-child){text-align: center;}table{border-spacing:0;border-collapse:seperate;}</style></head><body><table>';
	
	var table = data.tables[Object.keys(data.tables)[t]];
	output += `<tr><td class="center" colspan="${table[1].length}"><b>${data.name}</b> - Powered by CuppaZee.uk</td></tr>`
	if(!table) return '<table><tr><td>An Error Occured</td></tr></table>';
	for(var i = 0;i < table.length;i++){
		output += "<tr>";
		for(var j = 0;j < table[i].length;j++){
			output += `<td lvl="${table[i][j].lvl}"${table[i][j].colspan ? ' colspan="' + table[i][j].colspan + '"' : ''}>${table[i][j].val}</td>`;
		}
		output += "</tr>";
	}
	output += '</table></body></html>';
	return output;
}

global.greenieOutput = function(d){
	var output = {
		"name":d.name,
		"id":d.id,
		"requirements-user":{},
		"requirements-clan":{},
		"data":[]
	};
	var clan = {
		"name":"CLAN TOTAL"
	};

	for(var i = 0;i < Object.keys(d.basicData).length;i++){
		let name = Object.keys(d.basicData)[i];
		let obj = d.basicData(obj);
		output.data.push({
			"name":name
		});
		for(var j = 0;j < Object.keys(obj).length;j++){
			let k = Object.keys(obj)[j];
			let o = obj[k];
			clan[useful.reqnames[k]] = (clan[useful.reqnames[k]] || 0) + o;
			output.data[output.data.length][useful.reqnames[k]] = o;
		}
	}
	output.data.sort(function(a,b){
		return b["Total"]-a["Total"];
	});
	output.data.push(clan);

	return output;
}*/

global.reloadClan = async function(clan, force){
	if(!clanreload[clan] || clanreload[clan] < Date.now() - 60000){
		if(!clans[clan] || !clans[clan].timestamp){
			//discordMsg('ClanReloading','NoTimeStamp', Date.now(),'NotAvailable or NoTimeStamp');
			clans[clan] = await getClanData(clan);
			io.emit('clan',clans[clan]);
		} else if(clans[clan].timestamp < Date.now() - (10 * 60000)){
			//discordMsg('ClanReloading',clans[clan].timestamp, Date.now(),'15Mins');
			clans[clan] = await getClanData(clan);
			io.emit('clan',clans[clan]);
		} else if(force && clans[clan].timestamp < Date.now() - 60000){
			//discordMsg('ClanReloading',clans[clan].timestamp, Date.now(),'Force1Min');
			clans[clan] = await getClanData(clan);
			io.emit('clan',clans[clan]);
		} else {
			//discordMsg('ClanReloading',clans[clan].timestamp, Date.now(),'None',force ? 'Forced' : 'Not Forced');
		}
	} else {
		//discordMsg('ClanReloading','AlreadyReloading');
	}
	//return clans[clan];
}

global.socketFunction = async function(socket){
	socket.on('requestclan_'+boot,async function(e){
		reloadClan(e.id, e.force);
        if(clans[e.id] && clans[e.id].tables) io.emit('clan',clans[e.id]);
	});
	socket.on('requestuser_'+boot,async function(e){
		if(!users[e.id] || e.force){
			users[e.id] = await getUserData(e.id);
		}
		io.emit('user',{id: e.id, info: users[e.id]});
	});

	socket.on('getclanid_'+boot,async function(e){
		let x = await getClanId(e)
		discordMsg('AddingClan',e,x);
		socket.emit('addclan',x);
	});
};

global.pages = [
	{
		page: ['zee.moblox.co.uk','*'],
		async function(req,res){
			res.send('CuppaZee v2 is out. Go to <a href="//cuppazee.uk">cuppazee.uk</a>')
		}
	},
	{
		page: ['*','/'],
		async function(req, res){
			let h = req.headers;
			discordMsg('PageLoaded',`IP: ${h['x-appengine-user-ip']} | Region: ${h['x-appengine-region']} | City: ${h['x-appengine-city']} | Co-ords: ${h['x-appengine-citylatlong']} | Country: ${h['x-appengine-country']}`);
			if(!req.secure) return res.redirect('https://cuppazee.uk');
			if(req.query.c){
				res.send(page.replace('$%BOOTCODE%$',boot).replace('/*$JSCODE$*/',`var newclans = [${req.query.c}]; for(var i = 0;i < newclans.length;i++){if(!clanls.includes(newclans[i])){clanls.push(newclans[i])}};localStorage.clans = JSON.stringify(clanls);`));
			} else {
				res.send(page.replace('$%BOOTCODE%$',boot));
			}
		}
	},
	{
		page: ['*','/reload'],
		function(req, res){
			reloadMunzeeList();
			res.send('Reloading. Thank you!');
		}
	},
	{
		page: ['*','/zeeqrew'],
		function(req, res){
			if(!req.query.u) return res.send('No User defined in "u" get parameter.');
			zeeQRew(req.query.u).then(function(i){
				res.setHeader('Content-Type', 'application/json')
				res.send(JSON.stringify(i, null, 4));
			});
		}
	},
	{
		page: ['*','/gpa'],
		async function(req, res){
			if(req.query.x){
				res.send(JSON.stringify(await GPA(req.query.x)));
			} else {
				res.send('Missing "x" username get parameter (Put "?x=USERNAME" at the end of the url)');
			}
		}
	}/*,{
		page: ['*','/greenie'],
		async function(req, res){
			if(req.query.clan){
				let clan = await getClanId(req.query.clan);
				res.send(clans[clan] || await getClanData(clan));
			} else {
				res.send('Missing Clan Parameter');
			}
		}
	},{
		page: ['*','/check'],
		async function(req, res){
			if(req.query.clan){
				let clan = await getClanId(req.query.clan);
				res.send([!!clans[clan], clan]);
			} else {
				res.send('Missing Clan Parameter');
			}
		}
	},{
		page: ['*','/html'],
		async function(req, res){
			if(req.query.id){
				let clan = req.query.id;
				res.send(htmlClan(clans[clan] || await getClanData(clan),Number(req.query.table || '0')));
			} else {
				res.send('Missing ID Parameter');
			}
		}
	},{
		page: ['*','/json'],
		async function(req, res){
			if(req.query.id){
				let clan = req.query.id;
				res.send(await reloadClan(clan));
			} else {
				res.send('Missing ID Parameter');
			}
		}
	}*/,
	{
		page: ['*','/exec'],
		function(req, res){
			console.log(req.cookies);
			if(req.cookies.pw == config.password){
				res.send(eval(req.query.c || null));
			} else {
				res.send('No Perms');
			}
		}
	},
	{
		page: ['*','/await'],
		async function(req, res){
			console.log(req.cookies);
			if(req.cookies.pw == config.password){
				res.send(JSON.stringify([await eval(req.query.c || null)]));
			} else {
				res.send('No Perms');
			}
		}
	},
	{
		page: ['*','/awaitpost'],
		function(req, res){
			if(req.body.pw == config.password){
				res.send(eval(req.body.c || null));
			} else {
				res.send('No Perms');
			}
		}
	},
	{
		page: ['*','/postexec'],
		async function(req, res){
			if(req.body.pw == config.password){
				res.send(JSON.stringify([await eval(req.body.c || null)]));
			} else {
				res.send('No Perms');
			}
		}
	},
	{
		page: ['*','/setpage'],
		function(req, res){
			if(req.body.pw == config.password){
				page = req.body.c;
				res.send('Done');
			} else {
				res.send('No Perms');
			}
		}
	},{
		page: ['*','/auth'],
		function(req, res){
			if(req.query.code) {
				console.log(req.query.code);
				var options = {
					method: 'POST',
					uri: 'https://api.munzee.com/oauth/login',
					url: 'https://api.munzee.com/oauth/login',
					formData: {
						'client_id': config.client_id,
						'client_secret': config.client_secret,
						'grant_type': 'authorization_code',
						'code': req.query.code, 
						'redirect_uri': config.redirect_uri
					}
				};
				rp(options)
					.then(async function (body) {
						var data = JSON.parse(body).data;
						console.log(data);
						data.username = await getUName(data.user_id, data);
						access.doc(data.username).set({
							"access_token": data.token.access_token,
							"token_type": data.token.token_type,
							"expires": data.token.expires,
							"expires_in": data.token.expires_in,
							"refresh_token": data.token.refresh_token,
							"user_id": data.user_id,
							"username": data.username
						});
						discordMsg('UserAuthenticated',data.username);
						res.send(`<script>window.location.replace('https://cuppazee.uk/');</script>`);
					})
					.catch(async function (err) {
						res.send(`${err} - Error - Redirecting in 3 seconds<script>setTimeout(()=>{window.location.replace('https://zee.moblox.co.uk/')},3000);</script>`);
					});
			} else {
				res.send(`<script>window.location.replace('https://cuppazee.co.uk/');</script>`);
			}
		}
	}
]
